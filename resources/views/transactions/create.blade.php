@extends('layouts.app')

        @section('content')

        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Error!</strong> 
                <ul>
                    @foreach ($errors->all() as $error)
                        <li></li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div>
            <form action="/store" method="POST">
                @csrf
                
                <div class="form-group">
                    <label for="transaction_type">Type:</label>
                    <select name="transaction_type" id="transaction_type" class="form-control">
                        <option value="Food">Food</option>
                        <option value="Transport">Transport</option>
                        <option value="Grocery">Grocery</option>
                        <option value="Fuel">Fuel</option>
                        <option value="Others">Others</option>
                        <option value="Rent">Rent</option>
                        <option value="Utilities">Utilities</option>
                        <option value="Rent">Rent</option>
                        <option value="Savings">Savings</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="amount">Amount:</label>
                    <input type="text" name="amount" id="amount" value="" class="form-control">
                </div>

                <div class="form-group">
                    <label for="description">Description:</label>
                    <textarea name="description" id="description" cols="30" rows="10" class="form-control"></textarea>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Save</button>
                </div>


            </form>
        </div>
        @endsection