@extends('layouts.app')
        
        @section('content')

            <div>
                <form action="/create" method="POST">
                    @method('GET')
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Create Transaction</button>
                </form>
            </div>

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>Description</th>
                            <th>Type</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($transactions as $transaction)
                        <tr>
                            <td>{{ $transaction->updated_at }}</td>
                            <td>{{ $transaction->amount }}</td>
                            <td>{{ $transaction->description }}</td>
                            <td>{{ $transaction->transaction_type }}</td>
                            
                            <td>
                                <form action="/edit/{{ $transaction->id }}" method="POST">
                                    @csrf
                                    @method('GET')
                                    <button class="btn btn-default">Edit</button>
                                </form>
                            </td>
                            <td>
                                <form action="/delete/{{ $transaction->id }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-default">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endsection