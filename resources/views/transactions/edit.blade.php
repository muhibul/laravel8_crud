@extends('layouts.app')

        @section('content')

        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Error!</strong> 
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div>
            <form action="/update/{{ $transaction->id }}" method="POST">
                @csrf
                @method('PUT')
                
                <div class="form-group">
                    <label for="amount">Date:</label>
                    <input type="text" name="updated_at" id="updated_at" value="{{ $transaction->updated_at }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="transaction_type">Type:</label>
                    <select name="transaction_type" id="transaction_type" class="form-control">
                    @foreach ($type_list as $key => $value)
                        <option value="{{ $key }}" {{ ( $key == $transaction->transaction_type) ? 'selected' : '' }}>{{ $value }}</option>
                        
                    @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="amount">Amount:</label>
                    <input type="text" name="amount" id="amount" value="{{ $transaction->amount }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="description">Description:</label>
                    <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{ $transaction->description }}</textarea>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Update</button>
                </div>


            </form>
        </div>
        @endsection