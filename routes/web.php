<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TransactionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [TransactionController::class, 'index']);
Route::get('/create', [TransactionController::class, 'create']);
Route::post('/store', [TransactionController::class, 'store']);
Route::get('/edit/{id}', [TransactionController::class, 'edit']);
Route::put('/update/{id}', [TransactionController::class, 'update']);
Route::delete('/delete/{id}', [TransactionController::class, 'destroy']);

Route::resource('transactions', TransactionController::class);
