<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::all()->sortBy('created_at');

        return view('transactions.list', [
            'transactions' => $transactions,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transactions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'transaction_type' => 'required',
            'amount' => 'required'
        ]);

        $transaction = new Transaction();
        $transaction->transaction_type = $request->transaction_type;
        $transaction->amount           = $request->amount;
        $transaction->description      = $request->description;
        $transaction->updated_at       = Carbon::now()->timestamp;
        $transaction->save();

        return redirect()->route('transactions.index')->with('success', 'Transaction created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaction = Transaction::findOrFail($id);
        
        $type_list = array(
            'Food' => 'Food',
            'Transport' => 'Transport',
            'Grocery' => 'Grocery',
            'Fuel' => 'Fuel',
            'Others' => 'Others',
            'Rent' => 'Rent',
            'Utilities' => 'Utilities',
            'Education' => 'Education',
            'Savings' => 'Savings',
        );
        
        return view('transactions.edit', [
            'transaction' => $transaction,
            'type_list'   => $type_list,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'transaction_type' => 'required',
            'amount' => 'required'
        ]);

        $transaction = Transaction::find($id);
        $transaction->transaction_type = $request->transaction_type;
        $transaction->amount           = $request->amount;
        $transaction->description      = $request->description;
        // dd($transaction->description);

        $transaction->save();

        return redirect()->route('transactions.index')->with('success', 'Transaction updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction, $id)
    {
        $transaction->find($id)->delete();

        return redirect()->route('transactions.index')->with('success', 'Transaction deleted successfully');
    }
}
